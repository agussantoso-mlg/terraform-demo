# https://registry.terraform.io/providers/hashicorp/aws/latest/docs

provider "aws" {
  region = "ap-northeast-1"
  shared_credentials_file = "/home/atoz/.aws/credentials"
  profile = "astrust.indonesia"
}