locals {
  lambda_path = "lambda"
  lambda_func_path = "lambda/functions"
  lambda_layer_path = "lambda/layers"
  lambda_zip_path = "lambda/archives"
}

# lambda layers

data "archive_file" "testLayer1" {
  type             = "zip"
  source_dir       = "${local.lambda_path}/layers/testLayer1"
  output_path      = "${local.lambda_zip_path}/testLayer1.zip"
  output_file_mode = "0664"
}


# lambda functions

data "archive_file" "lambda_terraformtest" {
  type             = "zip"
  source_file      = "${local.lambda_func_path}/terraformtest.py"
  output_path      = "${local.lambda_zip_path}/terraformtest.zip"
  output_file_mode = "0664"
}
