#role
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = file("iam/roles/iam_for_lambda.json")
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole" {
    role       = aws_iam_role.iam_for_lambda.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
    role       = aws_iam_role.iam_for_lambda.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaRole" {
    role       = aws_iam_role.iam_for_lambda.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}

# layers

resource "aws_lambda_layer_version" "testLayer1" {
  filename            = data.archive_file.testLayer1.output_path
  source_code_hash    = data.archive_file.testLayer1.output_base64sha256
  layer_name          = "testLayer1"
  compatible_runtimes = ["python3.6"]
}

# functions

resource "aws_lambda_function" "lambda_terraformtest" {
  filename         = data.archive_file.lambda_terraformtest.output_path
  source_code_hash = data.archive_file.lambda_terraformtest.output_base64sha256
  function_name    = "terraformtest"
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = "terraformtest.handler"
  runtime          = "python3.6"
  layers           = [aws_lambda_layer_version.testLayer1.arn]
}